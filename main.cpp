#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
#include "RgbaPng.hpp"

using IntVec = std::vector<int>;
using UcharVec = std::vector<unsigned char>;
using UcharMat = std::vector<UcharVec>;
struct Point {
    int x, y;
    Point(int x, int y) : x(x), y(y) {}
    bool operator<(const Point& p)const { return x == p.x ? y < p.y : x < p.x; }
    Point operator+(const Point& p)const { return {x + p.x, y + p.y}; }
    bool isInGrid(int w, int h) { return 0 <= x&&x < w  &&  0 <= y&&y < h; }
};
using PointVec = std::vector<Point>;

int main()
{
    RgbaPng img;
    img.readFromFile("1.png");
    UcharMat P(img.h(), UcharVec(img.w())); //maximum of 256 pieces
    IntVec A;
    PointVec pMin, pMax;
    Point neighborDirections[]{ {1,0}, {0,1}, {-1,0}, {0,-1} };
    RgbaPng::Color WHITE{255, 255, 255, 255};
    for (unsigned y = 0; y < img.h(); ++y)
        for (unsigned x = 0; x < img.w(); ++x)
            if (!P[y][x] && !(img[y][x] == WHITE))
    {
        A.push_back(0);
        std::queue<Point> q;
        q.emplace(x, y);
        pMin.emplace_back(x, y);
        pMax.emplace_back(x, y);
        RgbaPng::Color color = img[y][x];
        while (!q.empty())
        {
            auto p = q.front(); q.pop();
            if (P[p.y][p.x]) continue;
            P[p.y][p.x] = A.size();
            end(pMin)[-1] = std::min(p, end(pMin)[-1]);
            end(pMax)[-1] = std::max(p, end(pMax)[-1]);
            end(A)[-1]++;

            for (auto const& dir : neighborDirections)
            {
                Point nb = p + dir;
                if (nb.isInGrid(img.w(), img.h()) && img[nb.y][nb.x] == color)
                    q.push(nb);
            }
        }
    }
    for (size_t i = 1; i < A.size(); ++i)
        std::cout << "size: " << A[i] << "\tmin ("
                  << pMin[i].x << "," << pMin[i].y << ")\tmax("
                  << pMax[i].x << "," << pMax[i].y << ")\n";
}
