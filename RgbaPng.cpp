#include "RgbaPng.hpp"

RgbaPng::Color::Color(int r, int g, int b, int a)
: r(r), g(g), b(b), a(a) {}

bool RgbaPng::Color::operator==(const Color& c)const
{
    return r == c.r && g == c.g && b == c.b && a == c.a;
}




RgbaPng::RgbaPng(int width, int height, RgbaPng::Color color)
: width(width), height(height), row_pointers(new png_bytep[height])
{
    for (int y = 0; y < height; y++)
    {
        row_pointers[y] = new png_byte[sizeof(Color) * width];
        for (int x = 0; x < width; x++)
            (*this)[y][x] = color;
    }
}

RgbaPng::~RgbaPng()
{
    for (png_uint_32 y = 0; y < height; y++) delete[] row_pointers[y];
    delete[] row_pointers;
}

RgbaPng::Color* RgbaPng::operator[](int y)
{
    return reinterpret_cast<Color*>(row_pointers[y]);
}

const RgbaPng::Color* RgbaPng::operator[](int y)const
{
    return reinterpret_cast<const Color*>(row_pointers[y]);
}

void RgbaPng::readFromFile(const std::string& fileName)
{
    FILE* fp = fopen(fileName.c_str(), "rb");

    /* check if this is png image */
    unsigned char sig[8];
    fread(sig, 1, 8, fp);
    if (png_sig_cmp(sig, 0, 8)) throw std::runtime_error(fileName + " is not a PNG file");

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    png_infop info_ptr = png_create_info_struct(png_ptr);
    setjmp(png_jmpbuf(png_ptr));
    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, 8);
    png_read_info(png_ptr, info_ptr);

    png_get_IHDR(
        png_ptr,
        info_ptr,
        &width, &height,
        &bitDepth,
        &colorType,
        NULL, NULL, NULL
    );

    row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
    for (png_uint_32 y = 0; y < height; y++)
        row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr, info_ptr));
    png_read_image(png_ptr, row_pointers);

    fclose(fp);
}

void RgbaPng::writeToFile(const std::string& fileName)const
{
    FILE* fp = fopen(fileName.c_str(), "wb");
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    png_infop info_ptr = png_create_info_struct(png_ptr);
    png_init_io(png_ptr, fp);

    /* write header */
    png_set_IHDR(
        png_ptr,
        info_ptr,
        width, height,
        bitDepth,
        colorType,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT,
        PNG_FILTER_TYPE_DEFAULT
    );
    png_write_info(png_ptr, info_ptr);

    /* write data */
    png_write_image(png_ptr, row_pointers);
    png_write_end(png_ptr, NULL);

    fclose(fp);
}
