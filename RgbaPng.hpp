#ifndef RGBAPNG_HPP
#define RGBAPNG_HPP

#include <png.h>
#include <cstdlib>
#include <string>
#include <stdexcept>

class RgbaPng
{
public:
    struct Color {
        png_byte r;
        png_byte g;
        png_byte b;
        png_byte a;
        Color(int r=0, int g=0, int b=0, int a=255);
        bool operator==(const Color&)const;
    };
public:
    RgbaPng() = default;
    RgbaPng(int width, int height, Color color=Color());
    ~RgbaPng();
    Color* operator[](int y);
    const Color* operator[](int y)const;
    void readFromFile(const std::string& fileName);
    void writeToFile(const std::string& fileName)const;
    unsigned w()const { return width; }
    unsigned h()const { return height; }
private:
    png_uint_32 width        = 0;
    png_uint_32 height       = 0;
    int         bitDepth     = 8;
    int         colorType    = PNG_COLOR_TYPE_RGBA;
    png_bytep*  row_pointers = nullptr;
};

#endif // RGBAPNG_HPP
